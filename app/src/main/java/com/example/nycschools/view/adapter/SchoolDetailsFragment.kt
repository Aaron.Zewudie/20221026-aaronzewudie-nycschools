package com.example.nycschools.view.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.nycschools.databinding.SchoolDetailsFragmentBinding
import com.example.nycschools.model.local.SchoolSatEntity

class SchoolDetailsFragment: Fragment() {

    private lateinit var entityDetail: SchoolSatEntity

    companion object {
        fun newInstance(data: SchoolSatEntity) = SchoolDetailsFragment().apply {
            entityDetail = data
        }
    }


    private lateinit var binding: SchoolDetailsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = SchoolDetailsFragmentBinding.inflate(
            inflater,
            container,
            false
        )

        initViews()
        return binding.root
    }

    private fun initViews() {
        binding.tvSchoolName.text = entityDetail.school_name
        binding.satTakers.text = entityDetail.num_of_sat_test_takers
        binding.tvMath.text = entityDetail.sat_math_avg_score
        binding.tvReading.text = entityDetail.sat_critical_reading_avg_score
        binding.tvWrite.text = entityDetail.sat_writing_avg_score

    }
}
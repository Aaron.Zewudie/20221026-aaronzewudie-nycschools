package com.example.nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.model.IRepository

import com.example.nycschools.model.UIState

import kotlinx.coroutines.*

import javax.inject.Inject

/**
 * DI (Dependency injection)
 * 2 types of Injections
 * Constructor injection.- Create the object along with the
 * needed dependencies.
 * Field injection.- Create the object at runtime, from a field references.
 * Is mandatory to use @Inject. (Android components and external libraries)
 */
class NYCViewModel @Inject constructor(private val repository: IRepository) : ViewModel() {

    private val _schoolSat = MutableLiveData<UIState>()
    val schoolSat: LiveData<UIState>
        get() = _schoolSat

    init {
        viewModelScope.launch {
            repository.getData().collect {
                _schoolSat.value = it
            }
        }
    }
}
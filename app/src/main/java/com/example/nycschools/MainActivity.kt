package com.example.nycschools

import android.app.UiAutomation
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.nycschools.databinding.ActivityMainBinding
import com.example.nycschools.di.NYCApplication
import com.example.nycschools.model.UIState
import com.example.nycschools.model.local.SchoolSatEntity
import com.example.nycschools.view.SchoolListDisplay
import com.example.nycschools.viewmodel.NYCViewModel
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private const val TAG = "MainActivity"

class MainActivity: AppCompatActivity() {

    @Inject lateinit var viewModel: NYCViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)
        NYCApplication.component.inject( this )

        viewModel.schoolSat.observe(this) {

            when (it){
                is UIState.Response -> initFragment(it.success)
                is UIState.Error -> showError(it.errorMessage)
                is UIState.Loading -> showLoading(it.isLoading)
                is UIState.Empty -> refreshData()
            }
        }
    }

    private fun refreshData() {
        Log.d(TAG, "refreshData: Empty")
    }

    private fun showLoading(loading: Boolean) {
        binding.progressBar.visibility = if (loading){
            View.VISIBLE
        }else {
           View.INVISIBLE
        }
    }

    private fun showError(errorMessage: String) {
        Snackbar.make(
            binding.content, errorMessage,
            Snackbar.LENGTH_INDEFINITE
        ).setAction("Dismiss"){
            Toast.makeText(this@MainActivity,
            "Dismissing Toast",
            Toast.LENGTH_SHORT).show();
        }.show()
    }

    private fun initFragment(data: List<SchoolSatEntity>) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SchoolListDisplay.newInstance(data))
            .commit()
    }
}
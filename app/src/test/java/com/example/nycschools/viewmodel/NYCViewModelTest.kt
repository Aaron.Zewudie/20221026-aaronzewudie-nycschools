package com.example.nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.model.IRepository
import com.example.nycschools.model.UIState
import com.google.common.truth.Truth.assertThat
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class NYCViewModelTest {

    @get:Rule val rule = InstantTaskExecutorRule()

    private val mockRepository = mockk<IRepository>(relaxed = true)
    private val testDispatcher = UnconfinedTestDispatcher()

    private lateinit var testObject: NYCViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        clearAllMocks()
        Dispatchers.resetMain()
    }

    @Test
    fun `get all data when initialising viewmodel returns success`() {
        every { mockRepository.getData() } returns flowOf(
            UIState.Response(listOf(mockk(), mockk(), mockk()))
        )
        val states = mutableListOf<UIState>()

        testObject = NYCViewModel(mockRepository)

        testObject.schoolSat.observeForever {
            states.add(it)
        }

        assertThat(states).hasSize(1)
        assertThat(states[0]).isInstanceOf(UIState.Response::class.java)
        assertThat((states[0] as UIState.Response).success).hasSize(3)
    }

    @Test
    fun `get all data when initialising viewmodel returns error`() {
        every { mockRepository.getData() } returns flowOf(
            UIState.Error("Error")
        )
        val states = mutableListOf<UIState>()

        val test = NYCViewModel(mockRepository)

        test.schoolSat.observeForever {
            states.add(it)
        }

        assertThat(states).hasSize(1)
        assertThat(states[0]).isInstanceOf(UIState.Error::class.java)
        assertThat((states[0] as UIState.Error).errorMessage).isEqualTo("Error")
    }

    @Test
    fun `get all data when initialising viewmodel returns loading`() {
        every { mockRepository.getData() } returns flowOf(
            UIState.Loading()
        )
        val states = mutableListOf<UIState>()

        val test = NYCViewModel(mockRepository)

        test.schoolSat.observeForever {
            states.add(it)
        }

        assertThat(states).hasSize(1)
        assertThat(states[0]).isInstanceOf(UIState.Loading::class.java)
        assertThat((states[0] as UIState.Loading).isLoading).isTrue()
    }

    @Test
    fun `get all data when initialising viewmodel returns empty`() {
        every { mockRepository.getData() } returns flowOf(
            UIState.Empty
        )
        val states = mutableListOf<UIState>()

        val test = NYCViewModel(mockRepository)

        test.schoolSat.observeForever {
            states.add(it)
        }

        assertThat(states).hasSize(1)
        assertThat(states[0]).isInstanceOf(UIState.Empty::class.java)
    }
}
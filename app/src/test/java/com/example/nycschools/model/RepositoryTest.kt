package com.example.nycschools.model

import com.example.nycschools.model.local.NYCDao
import com.example.nycschools.model.remote.NYCApi
import com.example.nycschools.model.remote.Network
import com.google.common.truth.Truth.assertThat
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class RepositoryTest {

    private val mockDao: NYCDao = mockk(relaxed = true)
    private val mockService = mockk<NYCApi>(relaxed = true)
    private val mockNetwork: Network = mockk(relaxed = true) {
        every { service }  returns mockService
    }
    private val testDispatcher = UnconfinedTestDispatcher()

    private lateinit var testObject: IRepository

    @Before
    fun setUp() {
        testObject = Repository(mockDao, mockNetwork, testDispatcher)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        clearAllMocks()
        Dispatchers.resetMain()
    }

    @Test
    fun `get data when response for schools and scores are success emits response from database`() = runTest {
        coEvery { mockDao.getPresentationData() } returns listOf(mockk(relaxed = true), mockk(relaxed = true), mockk(relaxed = true))
        coEvery { mockService.getRemoteSchoolList() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk(relaxed = true))
        }
        coEvery { mockService.getRemoteSat() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk(relaxed = true))
        }
        val states = mutableListOf<UIState>()

        testObject.getData().collect {
            states.add(it)
        }

        coVerify(exactly = 1) { mockDao.fetchSchoolList(any()) }
        coVerify(exactly = 1) { mockDao.fetchSatList(any()) }
        coVerify(exactly = 1) { mockDao.getPresentationData() }
        assertThat(mockDao.getPresentationData()).hasSize(3)
        assertThat(states).hasSize(2)
        assertThat(states[1]).isInstanceOf(UIState.Response::class.java)
        assertThat((states[1] as UIState.Response).success).hasSize(3)
    }

    @Test
    fun `get data when response for schools and scores are success emits empty response for sat scores`() = runTest {
        coEvery { mockDao.getPresentationData() } returns listOf(mockk(relaxed = true), mockk(relaxed = true), mockk(relaxed = true))
        coEvery { mockService.getRemoteSchoolList() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk(relaxed = true))
        }
        coEvery { mockService.getRemoteSat() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }
        val states = mutableListOf<UIState>()

        testObject.getData().collect {
            states.add(it)
        }

        coVerify(exactly = 0) { mockDao.fetchSchoolList(any()) }
        coVerify(exactly = 0) { mockDao.fetchSatList(any()) }
        coVerify(exactly = 0) { mockDao.getPresentationData() }
        assertThat(states).hasSize(2)
        assertThat(states[1]).isInstanceOf(UIState.Empty::class.java)
        assertThat((states[1] as UIState.Empty))
    }

    @Test
    fun `get data when response for schools and scores are success emits empty response for schools is null`() = runTest {
        coEvery { mockDao.getPresentationData() } returns listOf(mockk(relaxed = true), mockk(relaxed = true), mockk(relaxed = true))
        coEvery { mockService.getRemoteSchoolList() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }
        coEvery { mockService.getRemoteSat() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }
        val states = mutableListOf<UIState>()

        testObject.getData().collect {
            states.add(it)
        }

        coVerify(exactly = 0) { mockDao.fetchSchoolList(any()) }
        coVerify(exactly = 0) { mockDao.fetchSatList(any()) }
        coVerify(exactly = 0) { mockDao.getPresentationData() }
        assertThat(states).hasSize(2)
        assertThat(states[1]).isInstanceOf(UIState.Empty::class.java)
        assertThat((states[1] as UIState.Empty))
    }

    @Test
    fun `get data when response for schools and scores are failure emits error`() = runTest {
        coEvery { mockDao.getPresentationData() } returns listOf(mockk(relaxed = true), mockk(relaxed = true), mockk(relaxed = true))
        coEvery { mockService.getRemoteSchoolList() } returns mockk {
            every { isSuccessful } returns false
            every { body() } returns null
        }
        coEvery { mockService.getRemoteSat() } returns mockk {
            every { isSuccessful } returns false
            every { body() } returns null
            every { message() } returns "Error"
        }
        val states = mutableListOf<UIState>()

        testObject.getData().collect {
            states.add(it)
        }

        coVerify(exactly = 0) { mockDao.fetchSchoolList(any()) }
        coVerify(exactly = 0) { mockDao.fetchSatList(any()) }
        coVerify(exactly = 0) { mockDao.getPresentationData() }
        assertThat(states).hasSize(2)
        assertThat(states[1]).isInstanceOf(UIState.Error::class.java)
        assertThat((states[1] as UIState.Error).errorMessage).isEqualTo("Error")
    }

    @Test
    fun `get data when response for schools is success and scores is failure emits error`() = runTest {
        coEvery { mockDao.getPresentationData() } returns listOf(mockk(relaxed = true), mockk(relaxed = true), mockk(relaxed = true))
        coEvery { mockService.getRemoteSchoolList() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }
        coEvery { mockService.getRemoteSat() } returns mockk {
            every { isSuccessful } returns false
            every { body() } returns null
            every { message() } returns "Error"
        }
        val states = mutableListOf<UIState>()

        testObject.getData().collect {
            states.add(it)
        }

        coVerify(exactly = 0) { mockDao.fetchSchoolList(any()) }
        coVerify(exactly = 0) { mockDao.fetchSatList(any()) }
        coVerify(exactly = 0) { mockDao.getPresentationData() }
        assertThat(states).hasSize(2)
        assertThat(states[1]).isInstanceOf(UIState.Error::class.java)
        assertThat((states[1] as UIState.Error).errorMessage).isEqualTo("Error")
    }
}